// This file is part of the Acts project.
//
// Copyright (C) 2017 Acts project team
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "Acts/Plugins/DD4hep/ActsExtension.hpp"
#include "Acts/Plugins/DD4hep/IActsExtension.hpp"
#include "DD4hep/DetFactoryHelper.h"
#include "DetUtils.h"
#include "ACTFW/DD4hepDetector/DD4hepDetectorHelper.hpp"

using namespace std;
using namespace dd4hep;

/**
 Constructor for a cylindrical barrel volume, possibly containing layers and the
 layers possibly containing modules.
 */

static Ref_t
create_element(Detector& lcdd, xml_h xml, SensitiveDetector sens)
{
  xml_det_t x_det    = xml;
  string    det_name = x_det.nameStr();
  // Make DetElement
  DetElement cylinderVolume(det_name, x_det.id());
  // add Extension to Detlement for the RecoGeometry
  Acts::ActsExtension::Config volConfig;
  volConfig.isBarrel             = true;
  Acts::ActsExtension* detvolume = new Acts::ActsExtension(volConfig);
  cylinderVolume.addExtension<Acts::IActsExtension>(detvolume);
  // make Volume
  dd4hep::xml::Dimension x_det_dim(x_det.dimensions());
  Tube   tube_shape(x_det_dim.rmin(), x_det_dim.rmax(), x_det_dim.dz());
  Volume tube_vol(det_name, tube_shape, lcdd.air());  // air at the moment change later

  tube_vol.setVisAttributes(lcdd, x_det_dim.visStr());
  // go trough possible layers

  size_t layer_num = 0;

  size_t module_num = 0;
  for (xml_coll_t j(xml, _U(layer)); j; ++j) {
    xml_comp_t x_layer = j;
    double     l_rmin  = x_layer.inner_r();
    double     l_rmax  = x_layer.outer_r();
    double     dr      = x_layer.dr();
    int        layer_num=x_layer.id();
    int        double_sided=x_layer.attr<int> (_Unicode(double_side));
    double half_width_max=sqrt(l_rmax*l_rmax-l_rmin*l_rmin); 
    
    // Create Volume for Layer
    string layer_name = det_name + _toString((int)layer_num, "l_%d");
    Volume layer_vol(layer_name,Tube(l_rmin, l_rmax, x_layer.z()),lcdd.material(x_layer.materialStr()));
    DetElement lay_det(cylinderVolume, layer_name, layer_num);
    layer_vol.setVisAttributes(lcdd, x_layer.visStr());
    // go trough possible modules
    if (x_layer.hasChild(_U(module))) {
      double module_thickness_r=0.0;
      xml_comp_t x_module = x_layer.child(_U(module));
      //int        repeat   = x_module.repeat();
      //double     deltaphi = 2. * M_PI / repeat;
      if (x_module.width()>half_width_max) {std::cout<< "modules too big for current volume!!"<<std::endl;}     
      double delta_phi=2.*atan(x_module.width()/(l_rmin));
      int repeat = floor(2. * M_PI / delta_phi);
      if (repeat%2!=0){repeat-=1;}
     
      double deltaphi=2. * M_PI / repeat; 
      int zrepeat=(x_layer.z())/(x_module.length()*2.0);
       
      // std::cout<<"layer "<<layer_name<<" has "<<repeat<< " in x-y "<<std::endl;
      // std::cout<<"layer "<<layer_name<<" has "<<zrepeat<< " in z "<<std::endl;

      // Place the Modules in z
      // create the module volume and its corresponing component volumes first
      Volume mod_vol("module",Box(x_module.length(), x_module.width(), x_module.thickness()),lcdd.material(x_module.materialStr()));
      // Visualization
      mod_vol.setVisAttributes(lcdd, x_module.visStr());
      auto digiModule= FW::DD4hep::rectangleDigiModule(x_module.length(),x_module.thickness(),x_module.width(),sens.readout().segmentation());
      
      // the sensitive placed components to be used later to create the
      // DetElements
      std::vector<PlacedVolume> sensComponents;
      int                       comp_num = 0;
      std::shared_ptr<const Acts::DigitizationModule> digiComponent = nullptr;
      // go through module components

      for (xml_coll_t comp(x_module, _U(module_component)); comp; ++comp) {
        string     component_name = _toString((int)comp_num, "com_%d");
        xml_comp_t x_component    = comp;
        module_thickness_r+=x_component.thickness();
        Volume     comp_vol(component_name,Box(x_component.length(),x_component.width(),x_component.thickness()),lcdd.material(x_component.materialStr()));
        comp_vol.setVisAttributes(lcdd, x_component.visStr());

        digiComponent= FW::DD4hep::rectangleDigiModule(x_component.length(),x_component.thickness(),x_component.width(),sens.readout().segmentation());
        // make sensitive components sensitive
        if (x_component.isSensitive()) comp_vol.setSensitiveDetector(sens);

        // Place Component in Module
        Position     trans(x_component.x(), 0., x_component.z());
        PlacedVolume placedcomponent = mod_vol.placeVolume(comp_vol, trans);
        if (x_component.isSensitive()) sensComponents.push_back(placedcomponent);
        placedcomponent.addPhysVolID("component", comp_num);
        ++comp_num;
      }
     

      double r = 0.0;

      //std::cout<< "module thickness "<<module_thickness_r<<std::endl;
      double r_inner=l_rmin+0.01;
      double r_outer=l_rmin+module_thickness_r*3+0.01;
      double distance_=module_thickness_r*0.5; 
      if(distance_<0.01) {distance_=0;}
      
 
      int sides=(double_sided==2)?2:1;
      for (int k = -zrepeat; k <= zrepeat; k++) {
       // if (k % 2 == 0) r += dr;
        // Place the modules in phi
        for (int side=0;side<sides;side++){
            for (int i = 0; i < repeat; ++i) {
              if (double_sided==0){ 
                  r=(abs(k)%2)?r_inner:r_inner+distance_;
              }
              if (double_sided==1){
                  r=(i%2)?r_inner:r_outer;
                  if (r==r_inner){r=(abs(k)%2)?r_inner:r_inner+distance_;}
                  if (r==r_outer){r=(abs(k)%2)?r_outer:r_outer-distance_;}
              }
              if (double_sided==2){
                  r=(side)?r_inner:r_outer; 
                  if (r==r_inner){r=(abs(k)%2)?r_inner:r_inner+distance_;}
                  if (r==r_outer){r=(abs(k)%2)?r_outer:r_outer-distance_;} 
              }
              double phi = deltaphi / dd4hep::rad * i;
              //string layer_name_side=layer_name+_toString((int)side,"_S%d");
              string module_name
                  = layer_name+ _toString((int)module_num, "module%d");
              Position trans(r * cos(phi), r * sin(phi), k * x_module.length()*2);
              // create detector element
              DetElement mod_det(lay_det, module_name, module_num);
              // Set Sensitive Volumes sensitive
              if (x_module.isSensitive()) {
                mod_vol.setSensitiveDetector(sens);
                Acts::ActsExtension* moduleExtension=new Acts::ActsExtension(digiModule);
              }
              int comp_num = 0;
              for (auto& sensComp : sensComponents) {
                DetElement component_det(mod_det, "component", comp_num);
                Acts::ActsExtension* componentExtension= new Acts::ActsExtension(digiComponent);
                component_det.addExtension<Acts::IActsExtension>(componentExtension);
                component_det.setPlacement(sensComp);
                comp_num++;
              }

              // Place Module Box Volumes in layer
              //PlacedVolume placedmodule = layer_vol.placeVolume(mod_vol,Transform3D(RotationX(-0.5 * M_PI) * RotationZ(-0.5 * M_PI) * RotationX(phi - 0.6 * M_PI),trans));
              PlacedVolume placedmodule = layer_vol.placeVolume(mod_vol,Transform3D(RotationX(-0.5 * M_PI) * RotationZ(-0.5 * M_PI) * RotationX(phi - 0.5 * M_PI),trans));
              placedmodule.addPhysVolID("module", module_num*2);
              // assign module DetElement to the placed module volume
              mod_det.setPlacement(placedmodule);
              module_num++;
            }
        } 
      }
    }

    Acts::ActsExtension::Config layConfig;
    layConfig.isLayer = true;
    ///@todo re-enable material mapping
    // layConfig.materialBins1         = 100;
    // layConfig.materialBins2         = 100;
    // layConfig.layerMaterialPosition = Acts::LayerMaterialPos::inner;
    Acts::ActsExtension* detlayer = new Acts::ActsExtension(layConfig);
    lay_det.addExtension<Acts::IActsExtension>(detlayer);
    // Place layer volume
    PlacedVolume placedLayer = tube_vol.placeVolume(layer_vol);
    placedLayer.addPhysVolID("layer", layer_num);
    // Assign layer DetElement to layer volume
    lay_det.setPlacement(placedLayer);
  }

  // Place Volume
  Volume       mother_vol = lcdd.pickMotherVolume(cylinderVolume);
  PlacedVolume placedTube = mother_vol.placeVolume(tube_vol);
  placedTube.addPhysVolID("system", cylinderVolume.id());
  cylinderVolume.setPlacement(placedTube);
  return cylinderVolume;
}
DECLARE_DETELEMENT(cepc_Barrel, create_element)
